#include <iostream>
#include "../Constantes/Constantes.hpp"
using namespace std;

class SensDuJeu
{

private:
	//sens = 0 -> ++
	//sens = 1 -> --
	//J'ai besoin d'une classe qui va g�rer le sens du jeu et � chaque fois durant au d�but d'un tour je vais appeller getnumJoueur();
	//Quand la carte est un vallet, elle va appell� la m�thode inverserSens()
	bool sens = 0;
	int minCompteur = 0;
	const int maxCompteur = NbJoueurs - 1; //L'indice commence � 0
	int numJoueur = -1;

public:
	SensDuJeu();
	int *getNumJoueur();

	void inverserSens();
	int *reculerDunJoueur();
	void resetSensDuJeu();
};
