#include "Point.hpp"

int Point::getPoints(Joueur *joueur)
{
	int pointCumules = 0;
	Carte *carte;
	//Pour chaque cartes dans la main du joueur, j'additionne les points
	for(int nbCarteMain = 0; nbCarteMain < *joueur->getNbCartes(); nbCarteMain++)
	{
		carte = joueur->getPt_Carte(&nbCarteMain);

		ChiffreCarteEnum chiffre = *carte->getChiffre();

		if(chiffre == Roi || chiffre == Dame)
		{
			pointCumules += 10;
		}
		else if(chiffre == Vallet || chiffre == As || chiffre == 2)
		{
			pointCumules += 20;
		}
		else if(chiffre == Huit || chiffre == Joker)
		{
			pointCumules += 50;
		}
		else
		{
			pointCumules += chiffre + 1;
		}
	}
	return pointCumules;
}