#pragma once

#include <iostream>
#include <vector>

#include "../Constantes/Constantes.hpp"
#include "Carte.hpp"

using namespace std;

class Pioche
{
private:
	vector<Carte *> laPioche;

public:
	~Pioche();
	vector<Carte *> *getPt_laPioche();
	Carte *getPt_DerniereCarte();
	void supprimerDerniereCarte();
	void creerEtInsererLesCartes();
};