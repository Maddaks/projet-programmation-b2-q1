#pragma once

#include <iostream>
#include "../Enums/ChiffreCarteEnum.hpp"
#include "../Enums/SymboleCarteEnum.hpp"
#include "../Enums/CouleurCarteEnum.hpp"

using namespace std;

class Carte
{

private:
	ChiffreCarteEnum chiffre;
	SymboleCarteEnum symbole;
	CouleurCarteEnum couleur;

public:
	Carte();
	Carte(ChiffreCarteEnum chiffre, SymboleCarteEnum symbole, CouleurCarteEnum couleur);

	ChiffreCarteEnum *getChiffre();
	SymboleCarteEnum *getSymbole();
	CouleurCarteEnum *getCouleur();

	void setSymbole(SymboleCarteEnum symbole);
};