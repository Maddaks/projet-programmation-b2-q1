#include "Carte.hpp"

Carte::Carte() {}

Carte::Carte(ChiffreCarteEnum chiffre, SymboleCarteEnum symbole, CouleurCarteEnum couleur)
{
	this->chiffre = chiffre;
	this->symbole = symbole;
	this->couleur = couleur;
}

ChiffreCarteEnum *Carte::getChiffre() { return &chiffre; }
SymboleCarteEnum *Carte::getSymbole() { return &symbole; }
CouleurCarteEnum *Carte::getCouleur() { return &couleur; }

void Carte::setSymbole(SymboleCarteEnum symbole)
{
	this->symbole = symbole;
}
