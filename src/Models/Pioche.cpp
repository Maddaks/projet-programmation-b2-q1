#include "Pioche.hpp"

vector<Carte *> *Pioche::getPt_laPioche() { return &laPioche; }

Carte *Pioche::getPt_DerniereCarte()
{
	return laPioche.back();

}

void Pioche::supprimerDerniereCarte()
{
	laPioche.pop_back();
}

Pioche::~Pioche()
{

	//On détruit tous les *carte présent dans le vecteur
	for(int i = 0; i < laPioche.size(); i++)
	{
		//delete[] laPioche[i];
		laPioche[i] = nullptr;
	}
	laPioche.clear();
	laPioche.shrink_to_fit();
}
void Pioche::creerEtInsererLesCartes()
{
	for(int i = 1; i < TotalCarteDistincte + 1; i++)
	{
		for(int j = 0; j < TotalSymbole; j++)
		{
			//On rempli la liste par la fin
			laPioche.push_back(new Carte((ChiffreCarteEnum) i, (SymboleCarteEnum) j, (CouleurCarteEnum) (j % 2)));
		}
	}
}