#include "Joueur.hpp"
Joueur::Joueur() {}

vector<Carte *> *Joueur::getPT_LaMain()
{
	return &laMain;
}

bool Joueur::aPlusDeCartes()
{
	return laMain.empty();
}

Carte *Joueur::getPt_Carte(int *compteurCarte)
{
	return laMain.at(*compteurCarte);
}

void Joueur::retirerCarteDeLaMain(Carte *carte)
{
	int i;
	for(i = 0; i < laMain.size(); i++)
	{ //On recherche la position de la carte dans la liste pour la supprimer
		if(*carte->getChiffre() == *laMain[i]->getChiffre() && *carte->getSymbole() == *laMain[i]->getSymbole())
			break;
	}
	if(laMain.size() > 1)
		laMain.erase(laMain.begin() + i);
	else
		laMain.erase(laMain.begin());
}

void Joueur::ajouterCarteDansLaMain(Carte *carte)
{
	//Si le joueur � gagn� le round, il ne re�oit pas de cartes
	if(aGagneRound == false)
		laMain.push_back(carte);
}
bool *Joueur::getTour()
{
	return &tour;
}
int *Joueur::getNbCartes()
{
	nbCarte = laMain.size();
	return &nbCarte;
}
void Joueur::setTour(bool etat)
{
	tour = etat;
}
bool *Joueur::getaGagnerRound()
{
	return &aGagneRound;
}
void Joueur::setaGagnerRound(bool etat)
{
	aGagneRound = etat;
}

bool *Joueur::getPeutJouerTour()
{
	return &peutJouerTour;
}
void Joueur::setPeutJouerTour(bool etat)
{
	peutJouerTour = etat;
}

int *Joueur::getPoints()
{
	return &points;
}
void Joueur::setPoints(int points)
{
	this->points += points;
}

void Joueur::clearLaMain()
{
	laMain.clear();
}
