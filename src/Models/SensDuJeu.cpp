#include "SensDuJeu.hpp"

SensDuJeu::SensDuJeu() {}

int *SensDuJeu::getNumJoueur()
{

	if(sens == 0)//incr�mente
	{
		numJoueur++;

		if(numJoueur > maxCompteur)
		{
			numJoueur = minCompteur;
		}
	}
	else//d�cremente
	{
		numJoueur--;

		if(numJoueur < minCompteur)
			numJoueur = maxCompteur;
	}

	return &numJoueur;
}

void SensDuJeu::inverserSens()
{
	if(sens == 1)
		sens = 0;
	else
		sens = 1;
}

int *SensDuJeu::reculerDunJoueur()
{
	//Au cas où ça arrive dès le début du jeu
	if(numJoueur == -1)
		numJoueur = 0;

	if(sens == 0)//incr�mente
	{
		numJoueur--;

		if(numJoueur < minCompteur)
		{
			numJoueur = maxCompteur;
		}
	}
	else//d�cremente
	{
		numJoueur++;

		if(numJoueur > maxCompteur)
			numJoueur = minCompteur;
	}

	return &numJoueur;
}

void SensDuJeu::resetSensDuJeu()
{
	sens = 0;
	minCompteur = 0;
	numJoueur = -1;
}
