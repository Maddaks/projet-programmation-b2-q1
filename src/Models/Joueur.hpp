#pragma once

#include <iostream>
#include <vector>
#include "Carte.hpp"

using namespace std;

class Joueur
{
private:
	vector<Carte *> laMain;
	Carte carteADonner;
	bool tour = false;
	int nbCarte = 0;
	bool aGagneRound = false;
	bool peutJouerTour = true;

	int points = 0;

public:
	Joueur();
	vector<Carte *> *getPT_LaMain();
	bool aPlusDeCartes();
	Carte *getPt_Carte(int *);
	void retirerCarteDeLaMain(Carte *);
	void ajouterCarteDansLaMain(Carte *);
	bool *getTour();
	void setTour(bool);
	int *getNbCartes();

	bool *getaGagnerRound();
	void setaGagnerRound(bool);

	bool *getPeutJouerTour();
	void setPeutJouerTour(bool);

	int *getPoints();
	void setPoints(int points);

	void clearLaMain();
};
