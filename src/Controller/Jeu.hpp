#pragma once

#include <iostream>
#include <algorithm>
#include <ctime>

#include "../Models/Pioche.hpp"
#include "../View/Vue.hpp"
#include "../Models/Joueur.hpp"
#include "../Models/SensDuJeu.hpp"
#include "../Models/Point.hpp"

using namespace std;

class Jeu
{
private:
	Pioche pioche;

	vector<Carte *> *laPioche = pioche.getPt_laPioche();
	vector<Carte *> tasDeJeu;

	Joueur *joueur = nullptr;
	Joueur *joueurSuivant = nullptr;
	Carte *carteJoueur = nullptr;
	Carte *derniereCarteTas = nullptr;

	ChiffreCarteEnum *pt_chiffreJoueur = nullptr;
	SymboleCarteEnum *pt_symboleJoueur = nullptr;
	ChiffreCarteEnum *pt_chiffreTas = nullptr;
	SymboleCarteEnum *pt_symboleTas = nullptr;
	ChiffreCarteEnum *chiffre = nullptr;

	Vue vue;
	SensDuJeu sensDuJeu;
	Point gestionPoint;

	Joueur lesJoueurs[NbJoueurs];
	int nbJoueurSansCartes = 0;
	int compteurJoueur = 0;
	int compteurTour = 0;
	bool pointsAtteind = false;

	void demanderA_AfficherLaPioche();
	void demanderA_AfficherLeTasDeJeu();
	void demanderA_AfficherJoueurs();
	void InitialisationJeu();
	void initialisationAffichage();
	void distribuerCartesAuxJoueurs();
	void melangerLesCartes();
	void modifierSeedRandom();
	void affichageDuTour(int compteurTour = 0);
	void placerPremiereCartePiocheSurTasDeJeu();
	void lancementDuJeu();
	void poserCarteSurTasDeJeu(Carte *);
	void deleteVector(vector<Carte *> *);
	void setLaPioche();
	void tourDuJoueur();
	void ajouterCarteDuJoueurSurTas();
	void siCestUnJoker();
	void joueurPiocheUneCarte();
	void estCeQueCartePiochePeutEtrePoseeSurTas();
	bool peutPoserSaCarteSurTas();
	void onVerseLeTasDansLaPioche();
	void siJoueurNaPlusDeCarte();
	void demanderA_AfficherLaCarteChoisie();
	void verificationSiPiocheEstvide();
	void setInfosTour(int *);
	void actions_Speciales();
	void siCestUnHuit();
	void siCestUnVallet();
	void siCestUnAs();
	void siCestUnDeux();
	void SiCestUnDix();
	void choisirJoueurSuivant(int *);
	void donnerCarteAuJoueurSuivant(const int *);
	void DemanderA_Afficher4DerniereCarteJoueurSuivant();
	bool isRoundFini();
	void CalculDesPoints();
	void VerifPointsAtteind();
	void remiseAZeroAttributsJoueurs();
	void RemiseAZeroAttributJeu();
	void remiseAZero();
	void remiseAZeroMain(int *);

public:
	Jeu();
	~Jeu();
};
