#include "Jeu.hpp"
//La fonction 'random_shuffle' � besoin comme 3e param�tre de la r�f�rence d'une fonction random
int myRandom(int i)
{
	return std::rand() % i;
}
#pragma region Constructeur / destructeur

Jeu::Jeu()
{
	InitialisationJeu();
	initialisationAffichage();

	lancementDuJeu();
}

Jeu::~Jeu()
{

	deleteVector(&tasDeJeu);
	//delete laPioche;//Erreur dans visual studio 2019 mais pas vscode
	laPioche = nullptr;
	pt_chiffreJoueur = nullptr;
	pt_symboleJoueur = nullptr;
	pt_chiffreTas = nullptr;
	pt_symboleTas = nullptr;
	carteJoueur = nullptr;
	derniereCarteTas = nullptr;
}
void Jeu::deleteVector(vector<Carte *> *vector)
{
	for(int i = 0; i < vector->size(); i++)
	{
		//delete vector[i]; //Le debugguer met un point d'arrêt lui même suivi d'une erreur dans visual studio 2019 surêment dû au code derrière la lib <vector>
		vector->at(i) = nullptr;
	}
	vector->clear();
	//le .shrink_to_fit réduit l'utilisation de la mémoire en libérant la mémoire inutilisée
	vector->shrink_to_fit();
}
#pragma endregion

#pragma region Initialisation du jeu
void Jeu::InitialisationJeu()
{
	RemiseAZeroAttributJeu();
	modifierSeedRandom();
	setLaPioche();
	melangerLesCartes();
	distribuerCartesAuxJoueurs();
	placerPremiereCartePiocheSurTasDeJeu();
}
void Jeu::modifierSeedRandom()
{
	srand(unsigned(std::time(0))); //En commentaire pour le debug, d�sol� si j'ai oubli� de l'enlever
}
void Jeu::setLaPioche()
{
	pioche.creerEtInsererLesCartes();
}
void Jeu::melangerLesCartes()
{
	random_shuffle(laPioche->begin(), laPioche->end(), myRandom); //Fonctionne, #include <algortihm>
}
void Jeu::distribuerCartesAuxJoueurs()
{
	for(int i = 0; i < NbJoueurs; i++)
	{
		for(int j = 0; j < NbCartesDansLaMainALaBase; j++)
		{
			Carte *cartetmp = laPioche->back();
			lesJoueurs[i].ajouterCarteDansLaMain(laPioche->back());

			laPioche->pop_back();
		}
	}
}
void Jeu::placerPremiereCartePiocheSurTasDeJeu()
{
	//on repioche tant que c'est une carte spéciale

	do
	{
		//On pioche une carte de la pioche et on la met sur le tasDeJeU
		carteJoueur = pioche.getPt_DerniereCarte();

		pioche.supprimerDerniereCarte();

		poserCarteSurTasDeJeu(carteJoueur);

		//On récupère son chiffre pour le comparer aux cartes spéciales
		chiffre = carteJoueur->getChiffre();

	} while(*chiffre == Huit || *chiffre == Joker || *chiffre == Vallet || *chiffre == As || *chiffre == Deux);
}
void Jeu::RemiseAZeroAttributJeu()
{
	int nbCartePioche = laPioche->size();
	for(int i = 0; i < nbCartePioche; i++)
	{
		laPioche->at(i) = nullptr;
	}
	int nbCarteTas = tasDeJeu.size();
	for(int i = 0; i < nbCarteTas; i++)
	{
		tasDeJeu.at(i) = nullptr;
	}

	laPioche->clear();
	tasDeJeu.clear();
}
#pragma endregion
void Jeu::lancementDuJeu()
{

	while(pointsAtteind == false)
	{
		/*
		* A chaque boucle while
		Etapes:
			1)On choisit un joueur
			1.2)On choisit le joueur suivant
			2)On vérifie si la pioche est vide, si elle est vide on verse le tas dans la pioche
			3)On vérifie si le joueur à encore des cartes (0 cartes = round gagné)
			4)Si le joueur a gagné le round (0 cartes dans sa main)
			5)On indique qu'il va jouer son tour (tour = true)
			6)On affiche le tour après que le joueur ai joué
			7)Il joue son tour
			8)On affiche la carte choisie par le joueur
			9)On dit que son tour est fini
		*/

		while(compteurJoueur < NbJoueurs && isRoundFini() != true)
		{

			//1)
			joueur = &lesJoueurs[compteurJoueur];
			//1.2)
			choisirJoueurSuivant(&compteurJoueur);
			//2)
			verificationSiPiocheEstvide();
			//3)
			siJoueurNaPlusDeCarte();

			//4)
			if(*joueur->getaGagnerRound() == false && *joueur->getPeutJouerTour() == true)
			{
				compteurTour++;
				//5)
				joueur->setTour(true);
				//6)
				affichageDuTour(compteurTour);
				//7)
				tourDuJoueur();
				//8)
				vue.afficherCarteChoisie(carteJoueur);
				//9)
				joueur->setTour(false);
			}
			//sinon quand un joueur passera son tour, il ne pourra plus jouer
			joueur->setPeutJouerTour(true);
			compteurJoueur = *sensDuJeu.getNumJoueur();
		}

		CalculDesPoints();
		VerifPointsAtteind();

		affichageDuTour(compteurTour);
		vue.afficherPoints(lesJoueurs);
		remiseAZero();

	}
}
bool Jeu::isRoundFini()
{
	//Le round est fini quand un joueur n'a plus de carte
	for(int i = 0; i < NbJoueurs; i++)
	{
		if(lesJoueurs[i].aPlusDeCartes())
			return true;
	}
	return false;
}
void Jeu::choisirJoueurSuivant(int *compteurJoueur)
{
	//Si on arrive à la fin du tableau, alors on prend le premier joueur
	if((*compteurJoueur + 1) == NbJoueurs)
	{
		joueurSuivant = &lesJoueurs[0];
	}
	else
	{
		joueurSuivant = &lesJoueurs[(*compteurJoueur + 1)];
	}
}
void Jeu::verificationSiPiocheEstvide()
{
	if(laPioche->size() <= 1)
		onVerseLeTasDansLaPioche();
}
void Jeu::siJoueurNaPlusDeCarte()
{
	if(joueur->aPlusDeCartes())
	{ //Il gagne le round
		joueur->setaGagnerRound(true);

	}
}
void Jeu::tourDuJoueur()
{

	/*
	* BOUCLE : Pour chaque carte contenu dans la main du joueur
	Etapes:
		1)On récupère la première carte du tas et la carte du joueur
		2)On regarder s'il peut poser cette carte sur le tas
		3)S'il peut poser sa carte :
			3.1)On vérifie les actions spéciales des cartes que l'on peut poser
			3.1)On ajoute la carte du joueur sur le tas et on le supprime de sa main
		4)S'il n'a pas pu poser de carte :
			4.1)Il pioche une carte
			4.2)On vérifie si la carte qu'il vient de piocher peut être posée sur le tas
	*/
	bool aJoue = false;
	//BOUCLE
	for(int compteurCarte = 0; compteurCarte < *joueur->getNbCartes() && aJoue == false; compteurCarte++)
	{
		//1)
		setInfosTour(&compteurCarte);
		//2)
		bool peutJouer = peutPoserSaCarteSurTas();
		//3)
		if(peutJouer)
		{
			//3.1)
			actions_Speciales();
			//3.1)
			ajouterCarteDuJoueurSurTas();

			//on sort de la boucle for car il à joué sa carte
			aJoue = true;
			break;
		}
	}
	//4)
	if(aJoue == false)
	{
		//4.1)
		joueurPiocheUneCarte();
		//4.2)
		estCeQueCartePiochePeutEtrePoseeSurTas();
	}
}
void Jeu::setInfosTour(int *compteurCarte)
{
	carteJoueur = joueur->getPt_Carte(compteurCarte);
	derniereCarteTas = tasDeJeu.back();

	pt_chiffreJoueur = carteJoueur->getChiffre();
	pt_symboleJoueur = carteJoueur->getSymbole();

	pt_chiffreTas = derniereCarteTas->getChiffre();
	pt_symboleTas = derniereCarteTas->getSymbole();
}
bool Jeu::peutPoserSaCarteSurTas()
{
	return (*pt_chiffreJoueur) == (*pt_chiffreTas) || (*pt_symboleJoueur) == (*pt_symboleTas) || (*pt_chiffreJoueur) == Huit || (*pt_chiffreJoueur == Joker);
}
void Jeu::estCeQueCartePiochePeutEtrePoseeSurTas()
{
	bool peutJouer = peutPoserSaCarteSurTas();
	if(peutJouer)
	{
		ajouterCarteDuJoueurSurTas();
		siCestUnJoker();
	}
}
void Jeu::joueurPiocheUneCarte()
{

	//J'ajoute dans la main du joueur la dernière carte de la pioche
	joueur->getPT_LaMain()->push_back(laPioche->back());
	//Je la retire de la pioche
	laPioche->pop_back();
}
void Jeu::onVerseLeTasDansLaPioche()
{
	int nbCartetasDeJeu = tasDeJeu.size() - 1;
	for(int i = 0; i < nbCartetasDeJeu; i++)
	{
		//On ajoute la dernière carte du tasDeJeu dans laPioche
		laPioche->push_back(tasDeJeu.back());

		//On retire la dernière carte du tas de jeu
		//Et il faut laisser une carte pour que les joueurs puissent poser leur carte
		if(tasDeJeu.size() > 0)
		{
			tasDeJeu.pop_back();
		}
	}
}
void Jeu::ajouterCarteDuJoueurSurTas()
{
	//On ajoute la carte du joueur au tasDeJeu
	tasDeJeu.push_back(carteJoueur);
	//On retire la carte du joueur de sa main
	joueur->retirerCarteDeLaMain(carteJoueur);
}
void Jeu::poserCarteSurTasDeJeu(Carte *carte)
{
	tasDeJeu.push_back(carte);
}
void Jeu::CalculDesPoints()
{

	for(int i = 0; i < NbJoueurs; i++)
	{//On comptabilise les points de ceux qui n'ont pas gagné le round
		if(*lesJoueurs[i].getaGagnerRound() == false)
		{
			int pointsCumules = gestionPoint.getPoints(&lesJoueurs[i]);
			lesJoueurs[i].setPoints(pointsCumules);
		}

	}
}
void Jeu::VerifPointsAtteind()
{
	for(int i = 0; i < NbJoueurs; i++)
	{
		if(*lesJoueurs[i].getPoints() >= maxPoint)
		{
			pointsAtteind = true;
		}
	}
}
void Jeu::remiseAZero()
{
	remiseAZeroAttributsJoueurs();
	InitialisationJeu();
}
void Jeu::remiseAZeroAttributsJoueurs()
{
	for(int i = 0; i < NbJoueurs; i++)
	{
		lesJoueurs[i].setaGagnerRound(false);
		lesJoueurs[i].setPeutJouerTour(true);
		remiseAZeroMain(&i);
	}
	compteurJoueur = 0;
	compteurTour = 0;
}
void Jeu::remiseAZeroMain(int *i)
{

	int nbCarteLaMain = *lesJoueurs[*i].getNbCartes();
	for(int numCarte = 0; numCarte < nbCarteLaMain; numCarte++)
	{
		lesJoueurs[*i].getPT_LaMain()->at(numCarte) = nullptr;
	}
	lesJoueurs[*i].clearLaMain();
}
#pragma region Actions Speciales
void Jeu::actions_Speciales()
{
	siCestUnHuit();
	siCestUnJoker();
	siCestUnVallet();
	siCestUnAs();
	siCestUnDeux();
	SiCestUnDix();
}

void Jeu::siCestUnJoker()
{
	//Si on pose un joker, celui-ci prend la couleur de la dernière carte du tas
	if(*carteJoueur->getChiffre() == Joker)
	{
		carteJoueur->setSymbole(*derniereCarteTas->getSymbole());
		donnerCarteAuJoueurSuivant(&NbCarteADonnerJoker);
		vue.afficherMessage("**** Attaque : 4 cartes en plus pour le joueur suivant !");
	}
}
void Jeu::siCestUnHuit()
{
	if(*carteJoueur->getChiffre() == Huit)
	{
		carteJoueur->setSymbole((SymboleCarteEnum) (rand() % TotalSymbole));
	}
}

void Jeu::donnerCarteAuJoueurSuivant(const int *nbCartesADonner)
{
	for(int nbCartes = 0; nbCartes < *nbCartesADonner; nbCartes++)
	{
		//vu qu'on retire une carte à chaque fois, il faut vérifier si la pioche est vide ou non
		verificationSiPiocheEstvide();

		joueurSuivant->ajouterCarteDansLaMain(laPioche->back());

		laPioche->pop_back();
	}
}

void Jeu::siCestUnVallet()
{
	if(*carteJoueur->getChiffre() == Vallet)
	{
		joueurSuivant->setPeutJouerTour(false);
	}
}
void Jeu::siCestUnAs()
{
	if(*carteJoueur->getChiffre() == As)
	{
		sensDuJeu.inverserSens();
	}
}
void Jeu::siCestUnDeux()
{
	if(*carteJoueur->getChiffre() == Deux)
	{
		donnerCarteAuJoueurSuivant(&NbCarteADonnerDeux);
	}
}

void Jeu::SiCestUnDix()
{
	if(*carteJoueur->getChiffre() == Dix)
	{
		compteurJoueur = *sensDuJeu.reculerDunJoueur();
	}
}
#pragma endregion
#pragma region Affichage
void Jeu::affichageDuTour(int compteurTour)
{
	vue.afficherTour(&compteurTour);
	//Vu que "Pioche" est un tableau, ça veut dire que c'est un pointeur
	vue.afficherUnVector(laPioche, "Pioche");
	vue.afficherUnVector(&tasDeJeu, "Tas");
	demanderA_AfficherJoueurs();
}

void Jeu::demanderA_AfficherJoueurs()
{
	for(int i = 0; i < NbJoueurs; i++)
	{
		vue.afficherJoueur(&lesJoueurs[i], &i);
	}
}

void Jeu::initialisationAffichage()
{
	vue.afficherDebutPartie();
	affichageDuTour();
}

#pragma endregion
