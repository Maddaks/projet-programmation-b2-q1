#pragma once

#include <vector>
#include "../Models/Pioche.hpp"
#include "../Models/Joueur.hpp"

using namespace std;

class Vue
{
private:
	Carte *carte = nullptr;
	vector<Carte *> *laMain = nullptr;

	void afficherChiffre(Carte *carte);
	void afficherSymbole(Carte *carte);

public:
	Vue();
	~Vue();
	void afficherUnVector(vector<Carte *> *, const char *);
	void afficherJoueur(Joueur *, int *);
	void afficherDebutPartie();
	void afficherCarteChoisie(Carte *);
	void afficherMessage(const char *);
	void afficherTour(int *);
	void afficherPoints(Joueur *lesJoueurs);
};