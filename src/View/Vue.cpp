﻿#include "Vue.hpp"
Vue::~Vue()
{
	//delete carte; //-> Bug sous vs2019
	//carte = nullptr;
}

Vue::Vue() {}

void Vue::afficherDebutPartie()
{
	cout << "**** Début de la partie !" << endl
		<< endl;

	cout << "**** Début du round !" << endl;
}

void Vue::afficherMessage(const char *msg)
{
	cout << *msg << endl;
}

void Vue::afficherUnVector(vector<Carte *> *vector, const char *nomVector)
{
	cout << endl;
	cout << "- " << nomVector << " : ";

	for(int i = 0; i < vector->size(); i++)
	{
		//On récupère l'instance de la carte à l'emplacement i et on donne son adresse au pointeur
		carte = vector->at(i);

		afficherChiffre(carte);
		afficherSymbole(carte);
		cout << " ";
	}
	cout << endl;
}
void Vue::afficherJoueur(Joueur *joueur, int *numJoueur)
{
	laMain = joueur->getPT_LaMain();

	if(*joueur->getTour())
		cout << ">";
	else
		cout << "-";

	cout << " Joueur " << (*numJoueur + 1) << ": ";

	for(int i = 0; i < *joueur->getNbCartes(); i++)
	{
		carte = laMain->at(i);
		afficherChiffre(carte);
		afficherSymbole(carte);
		cout << " ";
	}
	cout << endl;
}

void Vue::afficherCarteChoisie(Carte *carte)
{
	cout << "**** Carte choisie : ";
	afficherChiffre(carte);
	afficherSymbole(carte);
	cout << endl
		<< "_______________________________________________________________________________________________________________";
}
void Vue::afficherTour(int *compteurTour)
{
	cout << *compteurTour;
}

void Vue::afficherPoints(Joueur *lesJoueurs)
{
	for(int i = 0; i < NbJoueurs; i++)
	{
		cout << endl << "Point du joueur[" << i << "] : " << *lesJoueurs[i].getPoints() << " points" << endl;
	}
}

void Vue::afficherChiffre(Carte *carte)
{
	ChiffreCarteEnum chiffre = *carte->getChiffre();

	switch(chiffre)
	{
	case As:
		cout << "As";
		break;
	case Vallet:
		cout << "V";
		break;
	case Dame:
		cout << "D";
		break;
	case Roi:
		cout << "R";
		break;
	case Joker:
		cout << "J";
		break;
	default:
		cout << chiffre;
		break;
	}
}
void Vue::afficherSymbole(Carte *carte)
{
	SymboleCarteEnum *symbole = carte->getSymbole();

	switch(*symbole)
	{
	case Pic:
		cout << "♠";
		break;
	case Trefle:
		cout << "♣";
		break;
	case Coeur:
		//TexteEnRouge();
		cout << "♥";
		//TexteEnNoir();
		break;
	case Carreau:
		//TexteEnRouge();
		cout << "♦";
		//TexteEnNoir();
		break;
	}
}
