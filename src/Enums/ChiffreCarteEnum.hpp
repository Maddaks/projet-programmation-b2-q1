#pragma once

enum ChiffreCarteEnum
{
	As = 1,
	Deux,
	Trois,
	Quatre,
	Cinq,
	Six,
	Sept,
	Huit,
	Neuf,
	Dix,
	Vallet,
	Dame,
	Roi,
	Joker
};