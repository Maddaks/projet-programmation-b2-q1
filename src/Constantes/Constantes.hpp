#pragma once

static const int TotalCarte = 56;
static const int TotalCarteDistincte = 14;
static const int TotalSymbole = 4;
static const int TotalCouleur = 2;
static const int NbCartesDansLaMainALaBase = 7;
static const int NbJoueurs = 4;
static const int maxPoint = 500;
static const int NbCarteADonnerJoker = 4;
static const int NbCarteADonnerDeux = 2;
